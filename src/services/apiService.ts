import Request from '@/utils/request';

export const account = (data) => Request({ url: '/url', method: 'GET', data });
export const homeReq = (data) => {
    return [
          {
            type: 1,
            name: '杭州未来科技城板块精品酒店出售',
            num: 'ZCJY20200965328',
            keywordList: ['资产出售', '经营中'],
            price: ['2500万']
          },
          {
            type: 2,
            name: '杭州未来科技城板块精品酒店出租',
            num: 'ZCJY20200965328',
            keywordList: ['资产出租', '经营中'],
            price: ['8万/月']
          },
          {
            type: 3,
            name: '杭州未来科技城板块精品酒店融资',
            num: 'ZCJY20200965328',
            keywordList: ['资产融资', '杭州'],
            price: ['2500万', '50%股权']
          },
          {
            type: 2,
            name: '杭州未来科技城板块精品酒店出租23',
            num: 'ZCJY20200965328',
            keywordList: ['资产出租', '经营中'],
            price: ['8万/月']
          },
        ]
};
