export default {
  pages: [
    'pages/index/index', 
    'pages/account/index',
    'pages/my/index',
    'pages/entrust/index',
    'pages/details/dealIndex', 
    'pages/details/financingIndex', 
    'pages/details/hireIndex', 
    'pages/details/brandIndex', 
    'pages/details/appointmentInspection', 
    'pages/assetDeal/index', 
    'pages/capitalLease/index', 
    'pages/investment/index', 
    'pages/brandJoining/index', 
    'pages/brandJoining/franchiseInformation', 
    'pages/brandJoining/brandEntry', 
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black',
  },
  tabBar: {
    color: "#FFFFFF",
    selectedColor: "#FFFFFF",
    backgroundColor: "#FFFFFF",
    borderStyle: 'white',
    list: [{
      pagePath: "pages/index/index",
      text: "首页"
    }, {
      pagePath: "pages/account/index",
      text: "职位"
    },
    {
      pagePath: "pages/my/index",
      text: "我的"
    }]
  },
  subPackages:[
      {
        "root": "pages/pagesA/",
        "pages": [
          'pageDome/index',
          'saleAssets/essential',
        ]
      }, {
        "root": "pages/pagesB/",        
        "pages": [
          'filterDome/index', 
          'cityData/index',
          'city/index',
          'eq/index',
        ]
      }
    ]
};
