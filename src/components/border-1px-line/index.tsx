import Taro from '@tarojs/taro'
import { Component } from 'react'
import { View } from '@tarojs/components'
import classNames from 'classnames'
import './index.scss'
type Props = {
  hasPB?: boolean  // 是否有padding-bottom
}
export default class SwiperBanner extends Component<Props, {}> {
  static defaultProps = {
    hasPB: true,
  }
  render() {
    const {hasPB} = this.props
    return (
      <View className={classNames(hasPB&&'line')} style='position: relative;'>
        <View className='line3'></View>
      </View>
    )
  }
}
