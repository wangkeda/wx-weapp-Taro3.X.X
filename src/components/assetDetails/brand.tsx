import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text, Swiper, SwiperItem, Block } from '@tarojs/components';

import './index.scss';

type Props = {
};
type State = {
  total: number,
  num: number,
}
// 资产出售
class Brand extends Component<Props, State> {
  readonly state: State = {
    num: 1,
    total: 0,
  }
  onHandle = ({ detail }) => {
    this.setState({
      num: detail.current + 1
    })
  }
  render() {
    return (
      <View className="brand-content">
        <View className="brand-line"></View>
        <View className="name-type">
          <Text className="brand-name">维也纳</Text>
          <Text className="brand-type">有限服务中档酒店</Text>
        </View>
        <Image className="brand-logo" mode="aspectFit" src="https://img-md.veimg.cn/meadinindex/M00/00/DC/wKgyH1sLWBOAV35NAABZGipYALE942.jpg"></Image>
      </View>
    );
  }
}
export default Brand;