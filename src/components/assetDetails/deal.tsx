import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text, Swiper, SwiperItem, Block } from '@tarojs/components';

import './index.scss';

type Props = {
};
type State = {
  total: number,
  num: number,
}
// 资产出售
class Deal extends Component<Props, State> {
  readonly state: State = {
    num: 1,
    total: 0,
  }
  onHandle = ({ detail }) => {
    this.setState({
      num: detail.current + 1
    })
  }
  render() {
    const { total, num } = this.state
    return (
      <View className="deal-content">
        <View className="details-swiper">
          <Swiper onChange={this.onHandle}>
            <SwiperItem>
              <Image mode="aspectFill" src={`${_IMG_URL}/dealcard.jpg`} />
            </SwiperItem>
            <SwiperItem>
              <Image mode="aspectFill" src={`${_IMG_URL}/dealcard.jpg`} />
            </SwiperItem>
          </Swiper>
          <View className="order-num">{`${num}/${total}`}</View>
        </View>
        <View className="row">
          <View className="number">
            <Text>编号：ZCJY20200965328</Text>
          </View>
          <View className="title">杭州未来科技城板块精品酒店出租</View>
          <View className="jg-cg-gq">
            <Text>2500万</Text>
            <Text>杭州</Text>
            <Text>经营中</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default Deal;