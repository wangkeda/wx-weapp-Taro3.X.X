import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { AtModal, AtModalContent, AtModalAction, AtFloatLayout, AtActionSheet, AtActionSheetItem,  } from "taro-ui"
import { View, Image, Text, Block, Button } from '@tarojs/components';
import PropertyPlaybill from '@/components/assetDetails/propertyPlaybill'
import { sysInfo } from '@/utils/utils'
import Navigator from '@/utils/navigate/Navigator'

import './index.scss';

type Props = {
  showType: string
};
type State = {
  modalOpened: boolean,
  isShare: boolean,
  isInfo: boolean,
  isBill: boolean,
  tipsText: string,
}
class FixdeBox extends Component<Props, State> {
  readonly state: State = {
    modalOpened: false,
    isShare: false,
    isInfo: false,
    isBill: false,
    tipsText: '投资顾问审核后，将为您提供该项目更详细的信息。请耐心等待',
  }
  $instance: any = Taro.getCurrentInstance();
  UNSAFE_componentWillMount() {
    // const events = new Taro.eventCenter();
    Taro.eventCenter.on(this.$instance.router.onShow, () => {
      const pages = Taro.getCurrentPages();
      const currPage = pages[pages.length - 1]; // 获取当前页面
      if (currPage.__data__.tipsType === "quantum") { // 获取值
        this.setState({
          tipsText: '投资顾问确认时间后，将与您联系，请耐心等待'
        }, () => {
          this.onAction('modalOpened', true);
        })
      } 
    })
  }
  componentWillUnmount() {
    // 卸载
    Taro.eventCenter.off(this.$instance.router.onShow)
  }
  // 申请查看详细信息
  getAskFor = (name: string, isOpened: boolean) => {
    this.setState({
      tipsText: '投资顾问审核后，将为您提供该项目更详细的信息。请耐心等待'
    }, () => {
      this.onAction(name, isOpened)
    })
  }
  onAction = (name: string, isOpened: boolean) => {
    const  data: any = {
      [name]: isOpened
    }
    name === 'isBill' && this.setState({
      isShare: false
    })
    this.setState(data)
  }
   onCallPhone = (phone: string) => {
    Taro.makePhoneCall({
      phoneNumber: phone
    })
  }
  copyWxNum = () => {
    Taro.setClipboardData({
      data: 'data1233',
    })
  }
  goYY = () => {
    Navigator.navigateTo({
      url: '//pages/details/appointmentInspection',
    })
    this.onAction('isInfo', false);
  }
  render() {
    const { showType } = this.props
    const { modalOpened, isShare, isInfo, isBill, tipsText } = this.state;
    return (
      <Block>
        {/* 海报 */}
        <View className={`billplay-box ${isBill}`} catchMove={true} >
          <View className="bg">
            <PropertyPlaybill  isHide={this.onAction.bind(this, 'isBill', false )} showType={showType} />
          </View>
        </View>
        {/* 申请提示 */}
        <AtModal isOpened={modalOpened} className="modal-success" closeOnClickOverlay={false}>
          <AtModalContent  >
            <View className="modal-sq">
              <Image className="success-icon" src={`${_IMG_URL}/success-icon.png`}></Image>
              <View>申请成功</View>
              <Text>
                {tipsText}
              </Text>
            </View>
          </AtModalContent>
          <AtModalAction><Button className="my-btn" onClick={this.onAction.bind(this, 'modalOpened', false)} >我知道了</Button> </AtModalAction>
        </AtModal>
        <AtActionSheet className="share-action" isOpened={isShare} cancelText='取消' onClose={this.onAction.bind(this, 'isShare', false )}>
          <View className="share-box">
            <Button className="icon-list" openType="share" >
              <Image src={`${_IMG_URL}/weixin-icon.png`}></Image>
              <View>分享给朋友</View>
            </Button>
            <Button className="icon-list" onClick={this.onAction.bind(this, 'isBill', true )}> 
              <Image src={`${_IMG_URL}/pengyouquan-icon.png`}></Image>
              <View>分享海报</View>
            </Button>
          </View>
        </AtActionSheet>
        <AtActionSheet isOpened={isInfo} className="sheet-info-box" cancelText='取消' onClose={this.onAction.bind(this, 'isInfo', false )} >
          <View >
            <View className="title">
              <Text>咨询该项目</Text>
              <View className='at-icon at-icon-close at-my' onClick={this.onAction.bind(this, 'isInfo', false )} ></View>
            </View>
            <View className="tz-gw">
              <Image className="gw-icon" mode="aspectFill" src={`${_IMG_URL}/dealcard.jpg`}></Image>
              <View className="name-wechat">
                <View >伍园英<Text> 投资顾问</Text></View>
                <View className="num">18956323301（电话与微信同号）</View>
              </View>
            </View>
            <View className="info-icon-box">
              <Button className="icon-list" onClick={this.onCallPhone.bind(this, '18956323301')} >
                <Image src={`${_IMG_URL}/dianhua-icon.png`}></Image>
                <View>拨打电话</View>
              </Button>
              <Button className="icon-list" onClick={this.copyWxNum}> 
                <Image src={`${_IMG_URL}/weixin-icon.png`}></Image>
                <View>复制号码</View>
                <View>添加微信</View>
              </Button>
              <Button className="icon-list" onClick={this.goYY}> 
                <Image src={`${_IMG_URL}/yuyue-icon.png`}></Image>
                <View>预约实地考察</View>
              </Button>
            </View>
          </View>
        </AtActionSheet>
        <View className="detail-fixed" id="bottom-fixed">
          <View className="detail-in">
            <Button className="detail-share" onClick={this.onAction.bind(this, 'isShare', true)}>
              <Image src={`${_IMG_URL}/share-icon.png`}></Image>
              <Text>分享</Text>
            </Button>
            {
              showType === "brand" ? (
                <Block>
                  <Button className="info-project brand active" onClick={this.onAction.bind(this, 'modalOpened', true)} >获取品牌加盟资料</Button>
                </Block>
              ) : (
                  <Block>
                    <Button className="get-detail" onClick={this.getAskFor.bind(this, 'modalOpened', true)}>申请查看详细信息</Button>
                    <Button className="info-project" onClick={this.onAction.bind(this, 'isInfo', true)} >咨询该项目</Button>
                  </Block>
                )
            }
          </View>
        </View>
      </Block>
    );
  }
}
export default FixdeBox;