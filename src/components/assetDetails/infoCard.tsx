import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text, Block } from '@tarojs/components';

import './index.scss';

type Props = {
  title: string,
  data: any,
  showType: number,
  showNum: number
};
type State = {
  moreBtn: boolean,
  list: any[]
}
// 物业、经营等信息
class InfoCard extends Component<Props, State> {
  readonly state: State = {
    moreBtn: false,
    list: []
  }
  static defaultProps = {
    data: []
  }
  componentDidMount() {
    const { data, showNum } = this.props;
    let ls = data
    if (data.length > showNum){
      ls = data.slice(0,showNum);
      this.setState({
        moreBtn: true,
      })
    }
    this.setState({
      list: ls
    })
  }
  onMore = () => {
    const { data } = this.props;
    this.setState({
      moreBtn: !this.state.moreBtn,
      list: data
    })
  }
  render() {
    let { title, showType, data } = this.props;
    const { moreBtn, list } = this.state;
    return (
      <View className="info-card">
        {
          showType === 1 && (
            <View className="bg">
              <View className="details-title">{title}</View>
              <View className="info-box">
                {
                  list.map((item) => {
                    return (
                      <View className="list">
                        <Text className="mc">{item.name}</Text>
                        <Text className="nr">{item.value}</Text>
                      </View>
                    )
                  })
                }
              </View>
              <View className={`get-more ${moreBtn}`} onClick={this.onMore}>
                <Text>查看更多</Text><View className="at-icon at-icon-chevron-down"></View>
              </View>
            </View>
          )
        }
        {
          showType === 2 && (
            <View className="bg">
              <View className="details-title">{title}</View>
              <View className="info-box">
                <Text className="des">{data[0].explain}</Text>
              </View>
            </View>
          )
        }
        {
           showType === 3 && (
            <View className="bg">
              <View className="details-title">{title}</View>
              <View className="year-list">
                <View className="year-box">
                  <View className="year">2019</View>
                  <View className="sr-lr">年收入：1200万    净利润：310万</View>
                  <Image className="bg-img" src={`${_IMG_URL}/sy-bg.png`}></Image>
                  <View className="round-box">
                    <Image src={`${_IMG_URL}/yuan-icon.png`}></Image>
                  </View>
                </View>
                <View className="year-box">
                  <View className="year">2019</View>
                  <View className="sr-lr">年收入：1200万    净利润：310万</View>
                  <Image className="bg-img" src={`${_IMG_URL}/sy-bg.png`}></Image>
                  <View className="round-box">
                    <Image src={`${_IMG_URL}/yuan-icon.png`}></Image>
                  </View>
                </View>
                <View className="year-box">
                  <View className="year">2019</View>
                  <View className="sr-lr">年收入：1200万    净利润：310万</View>
                  <Image className="bg-img" src={`${_IMG_URL}/sy-bg.png`}></Image>
                  <View className="round-box">
                    <Image src={`${_IMG_URL}/yuan-icon.png`}></Image>
                  </View>
                </View>

              </View>
            </View>
           )
        }
      </View>
    );
  }
}
export default InfoCard;