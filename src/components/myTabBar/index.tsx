import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import home from '@/assets/images/home.png'
import homeA from '@/assets/images/home-a.png'
import me from '@/assets/images/me.png'
import meA from '@/assets/images/me-a.png'
import add from '@/assets/images/add.png'
import addA from '@/assets/images/add-a.png'
import { AtTabBar } from 'taro-ui';
import Navigator from '@/utils/navigate/Navigator'
import './index.scss';
type Props = {
  current: number
};
class MyTabBar extends Component<Props, {}> {
  static defaultProps = {
    current: 0
  }
  onHandleClick = (e) => {
    let url = '/pages/index/index';
    e === 0 && (url = '/pages/index/index')
    e === 2 && (url = '/pages/my/index')
    e !== 1 && Taro.switchTab({
      url: url
    })
    e === 1 && Navigator.navigateTo({
      url: '/pages/entrust/index'
    })
  }
  render() {
    const { current } = this.props
    return (
     <AtTabBar
      className="my-tabBar"
      fixed
      iconSize={22}
      fontSize={11}
      selectedColor="#333333"
      color="#A6A9AC"
      tabList={[
          { title: '首页',  image: home, selectedImage: homeA},
          { title: '发布委托', image: add, selectedImage: addA},
          { title: '我的', image: me, selectedImage: meA}
        ]}
      onClick={this.onHandleClick.bind(this)}
      current={current}
      />
    );
  }
}
export default MyTabBar;