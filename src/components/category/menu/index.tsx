import Taro from '@tarojs/taro'
import React, { PureComponent } from 'react';
import { View } from '@tarojs/components'
import classNames from 'classnames'
import { item, subItem, categoryType } from '../index.d'
import './index.scss'

export type Props = {
  edit: boolean
  type: categoryType
  current: string
  list: item[]
  highLightMenus: string[]
  onClick: (args: subItem) => void
}

export type State = {}

export default class Menu extends PureComponent<Props, State> {
  static defaultProps = {
    type: 'Normal',
    current: '',
    list: [],
    edit: false,
  }

  handleClick = (item: subItem) => {
    this.props.onClick(item)
  }

  render() {
    const { type, current, list, highLightMenus } = this.props
    const isList = type === 'List'
    const isCity = type === 'City'

    return (
      <View className='cate-menu'>
        {list.map(item => {
          const active = item.code === current
          return (
            <View
              key={item.code}
              className={classNames(
                'cate-menu__item',
                isList && 'cate-menu__list',
                isCity && 'cate-menu__city',
                highLightMenus.indexOf(item.code) > -1 && 'cate-menu--active',
                active && 'cate-menu--active',
              )}
              onClick={this.handleClick.bind(this, item)}>
              {isList && active && <View className='cate-menu__list--active' />}
              {item.value}
              {!isList && active && <View className='cate-menu__item--active' />}
            </View>
          )
        })}
      </View>
    )
  }
}
