export type item = {
  code: string
  value: string
  sublist: subItem[]
}

export type subItem = {
  code: string
  value: string
}

export type categoryType = 'City' | 'Normal' | 'Grid' | 'List'
