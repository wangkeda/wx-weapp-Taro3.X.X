import Taro from '@tarojs/taro'
import React, { PureComponent } from 'react';
import { View } from '@tarojs/components'
import classNames from 'classnames'
import { item, categoryType, subItem } from '../index.d'
import './index.scss'

export type Props = {
  hasRegion: boolean
  edit: boolean
  type: categoryType
  list: item[] | subItem[]
  location: subItem | null
  current: subItem[]
  onClick: (args: subItem) => void
}

export type State = {}

export default class Content extends PureComponent<Props, State> {
  static defaultProps = {
    type: 'Normal',
    list: [],
    location: null,
    current: [],
    edit: false,
    hasRegion: false
  }

  handleClick = (item: subItem) => {
    this.props.onClick(item)
  }
  render() {
    const { type, list, location, current, hasRegion } = this.props
    const isCity = type === 'City'
    const isNormal = type === 'Normal'
    const isGrid = type === 'Grid'
    return (
      <View className='cate-content'>
        {/* {isCity && location && (
          <View className='cate-content__header'>
            <View>当前定位</View>
            {location.code && (
              <View
                className='cate-content__location'
                onClick={this.handleClick.bind(this, location)}
              >
                {location.value}
              </View>
            )}
            {!location.code && (
              <View className='cate-content__location'>{'定位失败' || location.value}</View>
            )}
          </View>
        )} */}
        <View
          className={classNames(
            isGrid && 'cate-content__grid',
            isCity && 'cate-content__city_in',
            isNormal && 'cate-content__normal',
            !location && 'cate-content--notHeader'
          )}
        >
          {(list as subItem[]).map(item => {
            const active = item.code === (current[0] && current[0].code)
            return (
              <View
                key={item.code}
                className={classNames(
                  isCity && 'cate-content-item__city',
                  isGrid && 'cate-content-item__grid',
                  isNormal && 'cate-content-item__normal',
                  active && 'cate-content-item--active',
                  current.length === 1 ? (current[0].code === item.code && 'cate-content-item--active') : (current
                    .map(x => x.code)
                    .toString()
                    .includes(item.code) && 'cate-content-item--active')
                )}
                onClick={this.handleClick.bind(this, item)}
              >
                {hasRegion && item.code.indexOf('0000') !== -1 ? '全部' : item.value}
              </View>
            )
          })}
        </View>
      </View>
    )
  }
}
