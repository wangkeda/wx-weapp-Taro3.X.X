import Taro from '@tarojs/taro'
import React, { PureComponent } from 'react';
import { View } from '@tarojs/components'
import classNames from 'classnames'
import { item, subItem } from '../index.d'
import '../menu/index.scss'

export type Props = {
  current: string
  list: item[]
  highLightMenus: string[]
  onClick: (args: subItem) => void
}

export type State = {}

export default class MenuList extends PureComponent<Props, State> {
  static defaultProps = {
    current: '',
    list: [],
  }

  handleClick = (item: subItem) => {
    this.props.onClick(item)
  }

  render() {
    const { current, list, highLightMenus } = this.props
    return (
      <View className='cate-menu'>
        {list.map(item => {
          const active = item.code === current
          return (
            <View
              key={item.code}
              className={classNames(
                'cate-menu__list',
                'cate-menu__list_industry',
                highLightMenus.indexOf(item.code) > -1 && 'cate-menu--active',

              )}
              onClick={this.handleClick.bind(this, item)}>
              {active && <View className='cate-menu__list--active' />}
              {item.value}
            </View>
          )
        })}
      </View>
    )
  }
}
