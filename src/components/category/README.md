# 类别选择器组件

## API

``` javascript

  const location = {
    code: '21',
    value: '杭州',
  }
  const options = [
    {
      code: '1',
      value: '热门',
      sublist: [
        {
          code: '21',
          value: '杭州',
        },
        {
          code: '31',
          value: '南京',
        },
        {
          code: '32',
          value: '苏州',
        },
        {
          code: '41',
          value: '广州',
        },
        {
          code: '42',
          value: '深圳',
        },
      ],
    },
    {
      code: '2',
      value: '浙江',
      sublist: [
        {
          code: '21',
          value: '杭州',
        },
        {
          code: '22',
          value: '宁波',
        },
      ],
    },
    {
      code: '3',
      value: '江苏',
      sublist: [
        {
          code: '31',
          value: '南京',
        },
        {
          code: '32',
          value: '苏州',
        },
      ],
    },
    {
      code: '4',
      value: '广东',
      sublist: [
        {
          code: '41',
          value: '广州',
        },
        {
          code: '42',
          value: '深圳',
        },
      ],
    },
    {
      code: '5',
      value: '上海',
      sublist: [
        {
          code: '51',
          value: '徐汇区',
        },
        {
          code: '52',
          value: '宝山区',
        },
      ],
    },
    {
      code: '6',
      value: '湖南',
      sublist: [
        {
          code: '61',
          value: '长沙市',
        },
        {
          code: '62',
          value: '张家界市',
        },
        {
          code: '63',
          value: '湘西',
        },
      ],
    },
  ]

  const defaultValue = [{
    code: '21',
    value: '杭州',
  },
  {
    code: '31',
    value: '南京',
  }]

  <Category
    edit
    max={5}
    type='City'
    location={location}
    defaultValue={defaultValue}
    options={options}
    onItemClick={this.handleClick}
    onChange={this.handleChange}
    onSave={this.handleSave}
  />
```

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| `edit` | 编辑模式，显示顶部选中展示框，type为`'City' | 'Normal' | 'List'` 有效 | `boolean` | `false` |
| `max` | 最多选择项，编辑模式下生效 | `number` | `3` |
| `type` | 类别选择器展示类型 | `'City' | 'Normal' | 'Grid' | 'List'`| `'Normal'` |
| `location` | 当前位置，`type`为`City`时生效，`{code:'',value:''}`时显示定位失败 | ` {code: string, value: string} | null ` | `null` |
| `radio` | 是否为单项选择 | `boolean` | `false` |
| `defaultValue` | 默认数据源，编辑模式下生效 | `{code: string, value: string}[]` | `[]` |
| `options` | 可选项数据源 | `{code: string, value: string, subList: []}[]` | `[]` |
| `onMenuClick` | 点击当前菜单项回调 | `(args: {code: string, value: string}) => void` | - |
| `onItemClick` | 点击当前内容项回调 | `(args: {code: string, value: string}) => void` | - |
| `onChange` | 改变内容时回调，编辑模式下生效 | `(args: {code: string, value: string}[]) => void` | - |
| `onSave` | 点击保存回调，编辑模式且`max > 1`时下生效 | `(args: {code: string, value: string}[]) => void` | - |
