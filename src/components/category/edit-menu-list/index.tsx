import Taro from '@tarojs/taro'
import React, { PureComponent } from 'react';
import { View, Image } from '@tarojs/components'
import classNames from 'classnames'
import { item, subItem, categoryType } from '../index.d'
import Border1PxLine from '@/components/border-1px-line'
import './index.scss'

export type Props = {
  edit: boolean
  type: categoryType
  currentItems: subItem[]
  list: item[]
  onClick: (args: subItem) => void
}

export type State = {}

export default class EditMenuList extends PureComponent<Props, State> {
  static defaultProps = {
    list: [],
    edit: true,
    type: 'List',
  }

  handleClick = (item: subItem) => {
    this.props.onClick(item)
  }

  render() {
    const { type, currentItems, list } = this.props
    const isList = type === 'List'
    return (
      <View className='cate-menu-edit'>
        {list.map(item => {
          const active =
            currentItems.length === 1
              ? currentItems[0].code === item.code
              : currentItems
                  .map(x => x.code)
                  .toString()
                  .includes(item.code)
          return (
            <View key={item.code}>
              <View
                className={classNames(isList && 'cate-menu-edit__list')}
                onClick={this.handleClick.bind(this, item)}
              >
                <View className='cate-menu-edit__list__icon'>
                  {active ? (
                    <Image className='cate-menu-edit__list--active' src='https://f3-v.veimg.cn/app/wx/ve/category-choose@2x.png' />
                  ) : (
                    <View className='cate-menu-edit__list__circle' />
                  )}
                </View>
                <View className='cate-menu-edit__list__text'>{item.value}</View>
              </View>
              <Border1PxLine hasPB={false} />
              {/* <View className='cate-menu-edit__divider' /> */}
            </View>
          )
        })}
      </View>
    )
  }
}
