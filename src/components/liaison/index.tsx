import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text, Block } from '@tarojs/components';

import './index.scss';

type Props = {
};
type State = {
}
// 项目联系人
class Liaison extends Component<Props, State> {
  readonly state: State = {
  }
  onCallPhone = (phone: string) => {
    Taro.makePhoneCall({
      phoneNumber: phone
    })
  }
  render() {
    return (
      <View className="liaison-box">
        <View className="bg">
          <View className="details-title">项目联系人</View>
          <View className="content" onClick={this.onCallPhone.bind(this, '18956323301')}>
            <Image className="user-icon" mode="aspectFill" src={`${_IMG_URL}/dealcard.jpg`} />
            <View className="name-ph">
              <Text className="name">魏先生</Text>
              <Text className="phone">18956323301</Text>
            </View>
            <View className="phone-icon">
              <Image src={`${_IMG_URL}/iphone-icon.png`}></Image>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
export default Liaison;