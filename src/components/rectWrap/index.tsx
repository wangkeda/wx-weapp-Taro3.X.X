import React, { Component } from 'react';
import { View, Image, Text } from '@tarojs/components';
import './rectWrap.scss';

type Props = {
  title: string;
  mb?: number;
};
class RectWrap extends Component<Props, {}> {
  render() {
    return (
      <View className="rectWrap">
        <View className="title">{this.props.title}</View>
      </View>
    );
  }
}
export default RectWrap;
