import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import Navigator from '@/utils/navigate/Navigator'
import './index.scss';

type Props = {
  data: {
    type: number,
    name: string,
    num: string,
    keywordList: string[],
    price: string[]
  }
};
// 品牌加盟
class BrandCard extends Component<Props, {}> {
  onHandleGo = (id) => {
    // 对应的详情页
    Navigator.navigateTo({
      url: `${id}`
    })
  }
  render() {
    const { data } = this.props
    return (
     <View className="common-asset-card brand-card" onClick={this.onHandleGo.bind(this, '11')}>
       <Image mode="aspectFit" src="https://img-md.veimg.cn/meadinindex/M00/00/DC/wKgyH1sLWBOAV35NAABZGipYALE942.jpg"></Image>
       <View className="asset-text">
         <View className="asset-h1">{data.name}</View>
         <View className="brand-type">国际高端酒店 · 全国性品牌</View>
         <View className="project-num">5000-8000㎡</View>
         <View className="jion-city">
           计划拓展：江苏、杭州、上海、江西、江西、江西、江西
         </View>
       </View>
     </View>
    );
  }
}
export default BrandCard;