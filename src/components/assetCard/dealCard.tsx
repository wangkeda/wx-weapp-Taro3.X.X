import React, { Component } from 'react';
import Taro from '@tarojs/taro';
import { View, Image, Text } from '@tarojs/components';
import Navigator from '@/utils/navigate/Navigator'
import './index.scss';

type Props = {
  data: {
    type: number,
    name: string,
    num: string,
    keywordList: string[],
    price: string[]
  }
};
// 资产出售
class DealCard extends Component<Props, {}> {
  onHandleGo = (id) => {
    // 对应的详情页
    Navigator.navigateTo({
      url: `${id}`
    })
  }
  render() {
    const { data } = this.props
    return (
     <View className="common-asset-card" onClick={this.onHandleGo.bind(this, '11')}>
       <Image mode="aspectFill" src={`${_IMG_URL}/dealcard.jpg`}></Image>
       <View className="asset-text">
         <View className="asset-h1">{data.name}</View>
         <View className="project-num">编号：{data.num}</View>
         <View className="keyword-list">
           {
             data.keywordList.map((item) => {
               return (
                 <Text>{item}</Text>
               )
             })
           }
         </View>
         <View className="price-num">
            {
             data.price.map((item) => {
               return (
                 <Text>{item}</Text>
               )
             })
           }
          </View>
       </View>
     </View>
    );
  }
}
export default DealCard;