import React, { Component } from 'react';
import { View, Text } from '@tarojs/components';
import FilterDropdown from '@/components/filterDropdown/index'
import data from './data';//筛选菜单数据
import './index.scss';

class FilterDome extends Component {
  state = {
    filterData: [],
    filterDropdownValue: [],
  }
  onReady() {
    
  }
  UNSAFE_componentWillMount() {
    this.setState({
      filterData: data,
      filterDropdownValue: [
        [0, 0, 0],				//第0个菜单选中 一级菜单的第1项，二级菜单的第1项，三级菜单的第3项
        [null, null],			//第1个菜单选中 都不选中
        [1],					//第2个菜单选中 一级菜单的第1项
        [[0], [1, 2, 7], [1, 0]],	//筛选菜单选中 第一个筛选的第0项，第二个筛选的第1,2,7项，第三个筛选的第1,0项
        [[0], [1], [1]],			//单选菜单选中 第一个筛选的第0项，第二个筛选的第1项，第三个筛选的第1项
      ]
    })
  }
  componentDidMount() {
    
  }
  // 对应 onShow
  componentDidShow() {
    console.log(`is componentDidShow`);
  }
  // 对应 onHide
  componentDidHide() {
    console.log(`is componentDidHide`);
  }
  // 对应 onPullDownRefresh，除了 componentDidShow/componentDidHide 之外，
  // 所有页面生命周期函数名都与小程序相对应
  onPullDownRefresh() {}
  // 对应 onPullDownRefresh
  onReachBottom() {}
  componentWillUnmount() {
    console.log(`is componentWillUnmount`);
  }
  // 对应 onError
  componentDidCatchError() {
    console.log(`is componentDidCatchError`);
  }
  //接收菜单结果
  onConfirm = (e) => {
    // console.log(e)
  }
  render() {
    const { filterData, filterDropdownValue } = this.state
    return (
      <View className='index'>
        <FilterDropdown filterData={filterData} defaultSelected={filterDropdownValue} updateMenuName={true} confirm={this.onConfirm} dataFormat="Object"></FilterDropdown>
      </View>
    )
  }
}
export default FilterDome;
