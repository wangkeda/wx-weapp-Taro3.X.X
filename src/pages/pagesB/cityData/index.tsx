import React, { Component ,useState,useEffect} from 'react'
import { View, Button, Text,Image,Input,ScrollView, Icon } from '@tarojs/components'
import Taro from "@tarojs/taro";
import './index.scss'
// import chinaData from './data'
import chinaData from "@/utils/areadata"

const initTabs = [
  {title:'请选择'},
  {title:'请选择'},
  {title:'请选择'}
]

export default ()=>{

  const [tabIndex,setTabIndex] = useState(0)

  const [screenW,setScreenW] =useState(375)

  const [zone,setZone] = useState({
    province:null,
    county:null,
    city:null
  })


  const animation = Taro.createAnimation({
    duration:300,
    timingFunction:'ease'
  })
  const [ani,setAni] = useState(animation)
  const navigateTo = (url) => {
    Taro.navigateTo({ url });
  }


  useEffect(()=>{
    const info = Taro.getSystemInfoSync()
    // console.log(info)
    const { screenWidth } = info
    setScreenW(screenWidth)

    console.log(chinaData)
  },[])


  useEffect(()=>{
    animation.translateX(-screenW * tabIndex).step()
    setAni(animation.export())
  },[tabIndex])




  function renderPageView(pageNum){
    let data = []
    if(pageNum === 0){
      data = chinaData
    }else if(pageNum === 1){
      if(zone.province){
        data = zone.province.children
      }
    }else if(pageNum === 2){
      if(zone.county){
        data = zone.county.children
      }
    }

    return (
      <View className={'list'} >

        {
          data.map((item,index)=>{

            let isSelect = false
            if(pageNum === 0 && zone.province ){
              isSelect = zone.province.value === item.value
            }else if(pageNum === 1 && zone.county){
              isSelect = zone.county.value === item.value
            }else if(pageNum === 2 && zone.city){
              isSelect = zone.city.value === item.value
            }

            return (
              <View className={isSelect ? 'list-item list-item-select' : 'list-item'}
                    key={index}
                    onClick={()=>{
                      console.log(item)
                      if(pageNum === 0) {
                        if(!zone.province || zone.province.value !== item.value){
                          setZone({
                            province:item,
                            county:null,
                            city:null
                          })
                          setTabIndex(1)
                        }
                      }else if(pageNum === 1){
                        if(!zone.county || zone.county.value !== item.value){
                          setZone(prevState => ({
                            ...prevState,
                            county:item,
                            city:null
                          }))
                          setTabIndex(2)
                        }
                      }else if(pageNum === 2){
                        if(!zone.city || zone.city.value !== item.value){
                          setZone(prevState => ({
                            ...prevState,
                            city:item
                          }))
                        }
                      }


                    }}
              >
                <Text>{item.label}</Text>
                <Icon size='20' type={isSelect ? `success` : `cancel`} />
              </View>
            )
          })
        }

      </View>
    )
  }


  function getTitle(index) {
    if(index === 0){
      return zone.province ? zone.province.label : '请选择'
    }
    if(index === 1){
      return zone.county ? zone.county.label : '请选择'
    }
    if(index === 2){
      return zone.city ? zone.city.label : '请选择'
    }
  }

  return (
    <View>

      <View className={'tabbar'} >

        {
          initTabs.map((item,index)=>{
            const isSelect = index === tabIndex

            return (
              <View className={isSelect ? 'tabbar-item select' : 'tabbar-item'}
                    key={index}
                    onClick={()=>{
                      console.log(index)
                      if(index > 0 && !zone.province) return
                       if(index === 2 && !zone.county) return;


                      setTabIndex(index)

                    }}
              >
                {getTitle(index)}
              </View>
            )
          })
        }
      </View>


      <View className={'tab'}
            animation={ani}
            style={{width:screenW * initTabs.length}}
      >
        {renderPageView(0)}
        {renderPageView(1)}
        {renderPageView(2)}
      </View>



    </View>
  )
}

