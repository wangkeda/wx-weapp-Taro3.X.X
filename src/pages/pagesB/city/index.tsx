import React, { Component, ComponentClass } from 'react';
import { View } from '@tarojs/components';
import { Props, State } from '@/pages/index/type'
import Navigator from '@/utils/navigate/Navigator';
import Category from '@/components/category/index'
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
class EqTest extends Component{
  componentDidMount() {
    console.log(this.props);
  }
  componentDidShow() {
  }
  handleClick = () => {

  }
  handleChange = () => {

  }
  handleSave = (args) => {
    console.log(args);
  }
  render() {
    // const { value } = this.props;
    const location = {
      code: '21',
      value: '杭州',
    }
    const options = [
      {
        code: '1',
        value: '热门',
        sublist: [
          {
            code: '21',
            value: '杭州',
          },
          {
            code: '31',
            value: '南京',
          },
          {
            code: '32',
            value: '苏州',
          },
          {
            code: '41',
            value: '广州',
          },
          {
            code: '42',
            value: '深圳',
          },
        ],
      },
      {
        code: '2',
        value: '浙江',
        sublist: [
          {
            code: '21',
            value: '杭州',
          },
          {
            code: '22',
            value: '宁波',
          },
        ],
      },
      {
        code: '3',
        value: '江苏',
        sublist: [
          {
            code: '31',
            value: '南京',
          },
          {
            code: '32',
            value: '苏州',
          },
        ],
      },
      {
        code: '4',
        value: '广东',
        sublist: [
          {
            code: '41',
            value: '广州',
          },
          {
            code: '42',
            value: '深圳',
          },
        ],
      },
      {
        code: '5',
        value: '上海',
        sublist: [
          {
            code: '51',
            value: '徐汇区',
          },
          {
            code: '52',
            value: '宝山区',
          },
        ],
      },
      {
        code: '6',
        value: '湖南',
        sublist: [
          {
            code: '61',
            value: '长沙市',
          },
          {
            code: '62',
            value: '张家界市',
          },
          {
            code: '63',
            value: '湘西',
          },
        ],
      },
    ]

    const defaultValue = []
    return (
      <View>
        <Category
          edit
          max={5}
          type='City'
          location={{code:'',value:''}}
          defaultValue={defaultValue}
          options={options}
          onItemClick={this.handleClick}
          onChange={this.handleChange}
          onSave={this.handleSave}
        />
      </View>
    );
  }
}

export default connect()(EqTest) as ComponentClass;
