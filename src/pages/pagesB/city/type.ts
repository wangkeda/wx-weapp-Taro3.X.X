export type StoreState = {
  // resume: IResumeStore
  loading: boolean
  options: IOptionsStore
  info: IInfoStore
  location: ILocationStore
  currentCity: ISearchPageStore
}

export type StoreDispatch = {
  saveData: ({ current_location: string }) => void
  changeSearchCity: (params: object) => void,
}

export type OwnProps = {}

export type Props = StoreState & StoreDispatch & OwnProps
