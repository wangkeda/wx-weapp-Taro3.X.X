import React, { Component, ComponentClass } from 'react';
import { View } from '@tarojs/components';
import { Props, State } from '@/pages/index/type'
import Navigator from '@/utils/navigate/Navigator';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
class EqTest extends Component<IProps, OwnState & State> {
  componentDidMount() {
    console.log(this.props);
  }
  componentDidShow() {
  }
  render() {
    // const { value } = this.props;
    return (
      <View>
        eq: 
      </View>
    );
  }
}
const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(EqTest) as ComponentClass;
