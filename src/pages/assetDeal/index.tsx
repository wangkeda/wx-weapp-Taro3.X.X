import React, { Component, ComponentClass } from 'react';
import { Block, View } from '@tarojs/components';
import { Props, State } from '@/pages/assetDeal/type'
import Navigator from '@/utils/navigate/Navigator';
import DealCard from '@/components/assetCard/dealCard';
import FilterDropdown from '@/components/filterDropdown/index'
import data from './data';//筛选菜单数据
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import './index.scss';

// @connect(() => ({
// }))
class AssetDeal extends Component<Props, State> {
  readonly state: State = {
    filterData: [],
    filterDropdownValue: [],
  }
  UNSAFE_componentWillMount() {
    this.setState({
      filterData: data,
      filterDropdownValue: [
        [0, 0, 0],				//第0个菜单选中 一级菜单的第1项，二级菜单的第1项，三级菜单的第3项
        [null],					//第2个菜单选中 一级菜单的第1项
        [null],			//单选菜单选中 第一个筛选的第0项，第二个筛选的第1项，第三个筛选的第1项
      ]
    })
  }
  componentDidMount() {
    
  }
  componentDidShow() {
    
  }
  //接收菜单结果
  onConfirm = (e) => {
    // console.log(e)
  }
  render() {
    const { filterData, filterDropdownValue } = this.state
    return (
      <Block>
        <FilterDropdown filterData={filterData} defaultSelected={filterDropdownValue} updateMenuName={true} confirm={this.onConfirm} dataFormat="Object"></FilterDropdown>
        <View className="filter-list asset-list">
          <DealCard data={{
              type: 1,
              name: '希尔顿',
              num: 'ZCJY20200965328',
              keywordList: ['资产出售', '经营中'],
              price: ['2500万']
            }} />
        </View>
      </Block>
      
    );
  }
}

export default connect()(AssetDeal) as ComponentClass;
