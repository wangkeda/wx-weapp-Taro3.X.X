import React, { Component, ComponentClass } from 'react';
import { View, Block } from '@tarojs/components';
import { Props, State } from '@/pages/investment/type'
import Navigator from '@/utils/navigate/Navigator';
import FilterDropdown from '@/components/filterDropdown/index'
import FinancingCard from '@/components/assetCard/hireCard';
import data from './data';//筛选菜单数据
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';

import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
class Investment extends Component<IProps, OwnState & State> {
  readonly state: State = {
    filterData: [],
    filterDropdownValue: [],
  }
  UNSAFE_componentWillMount() {
    this.setState({
      filterData: data,
      filterDropdownValue: [
        [0, 0, 0],				//第0个菜单选中 一级菜单的第1项，二级菜单的第1项，三级菜单的第3项
        [null],					//第2个菜单选中 一级菜单的第1项
        [null],			//单选菜单选中 第一个筛选的第0项，第二个筛选的第1项，第三个筛选的第1项
      ]
    })
  }
  componentDidMount() {
    console.log(this.props);
  }
  componentDidShow() {
  }
  onConfirm = () => {

  }
  render() {
    const { filterData, filterDropdownValue } = this.state
    return (
      <Block>
        <FilterDropdown filterData={filterData} defaultSelected={filterDropdownValue} updateMenuName={true} confirm={this.onConfirm} dataFormat="Object"></FilterDropdown>
        <View className="filter-list">
          <FinancingCard data={{
              type: 3,
              name: '希尔顿',
              num: 'ZCJY20200965328',
              keywordList: ['资产融资', '经营中'],
              price: ['1000万', '50%股权']
            }} />
        </View>
      </Block>
    );
  }
}

export default connect()(Investment) as ComponentClass;
