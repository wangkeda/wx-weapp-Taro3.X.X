export type StoreDispatch = {
  dataStr: ({
    payload: {
      star: number
    },
    callback: object,
  }) => void
}
export type StoreState = {
  homeData: {
    list: number[],
    star: number
  }
}
export type Props = StoreDispatch & StoreState

export type State = {
  filterDropdownValue: any[],
  filterData: any[],
}