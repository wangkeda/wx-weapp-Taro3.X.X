import React, { Component, ComponentClass } from 'react';
import { connect } from 'react-redux';
import Taro, {getCurrentInstance} from '@tarojs/taro';
import Navigator from '@/utils/navigate/Navigator'
import { Props, State } from '@/pages/my/type'
import { View, Text, Image, Button, Block } from '@tarojs/components';
import MyTabBar from '@/components/myTabBar';

class Index extends Component<Props, State> {
  state: State = {
    me: '哈哈哈',
    dt: 1
  }
  UNSAFE_componentWillMount() {
  }

  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {
    Taro.hideTabBar()
  }

  componentDidHide() {}
  onNavToSubS = () => {
    this.props.dataStr({
      payload: {
        star: 3,
      }, 
      callback: (res) => {
        console.log(res);
      }
    });
    // Navigator.navigateTo({
    //   url: '/pages/account/index'
    // });
  };
  onGoPage = (url) => {
    Navigator.navigateTo({
      url: url
    });
  };
  onGetPhoneNumber = (e) => {
    console.log(e);
  }
  render() {
    return (
      <Block>
        <View className="index">
          <Text>my</Text>
        </View>
        <MyTabBar current={2} />
      </Block>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    homeData: state.homeData
  }
}
const mapDispatchToProps = (dispatch) => ({
  dataStr: ({ payload, callback }) =>  dispatch({
    type: 'homeData/dataStr',
    payload: payload,
    callback: callback
  }),
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index) as ComponentClass
