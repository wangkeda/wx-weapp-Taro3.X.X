import React, { Component, ComponentClass } from 'react';
import { Block, View } from '@tarojs/components';
import { Props, State } from '@/pages/brandJoining/type'
import Navigator from '@/utils/navigate/Navigator';
import FilterDropdown from '@/components/filterDropdown'
import BrandCard from '@/components/assetCard/brandCard'
import data from './data';//筛选菜单数据;
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
class BrandJoining extends Component<IProps, OwnState & State & filterDropdown> {
  readonly state: State = {
    filterData: [],
    filterDropdownValue: [],
    addBtnType: false,
  }
  UNSAFE_componentWillMount() {
    this.setState({
      filterData: data,
      filterDropdownValue: [
        [[null]],				//第0个菜单选中 一级菜单的第1项，二级菜单的第1项，三级菜单的第3项
        [[null]],					//第2个菜单选中 一级菜单的第1项
        [null],			//单选菜单选中 第一个筛选的第0项，第二个筛选的第1项，第三个筛选的第1项
      ]
    })
  }
  componentDidMount() {
  }
  componentDidShow() {
  }
  onConfirm = () => {

  }
  onAdd = () => {
    this.setState({
      addBtnType: !this.state.addBtnType
    })
  }
  goPage = (url) => {
    Navigator.navigateTo({
      url: url
    })
  }
  render() {
    const { filterData, filterDropdownValue, addBtnType } = this.state
    console.log(addBtnType);
    return (
      <Block >
        <FilterDropdown filterData={filterData} defaultSelected={filterDropdownValue} updateMenuName={true} confirm={this.onConfirm} dataFormat="Object"></FilterDropdown>
        <View className="filter-list brand-list">
          <BrandCard data={{
              type: 1,
              name: '希尔顿',
              num: 'ZCJY20200965328',
              keywordList: ['资产出售', '经营中'],
              price: ['2500万']
            }} />
          <View className="add-box">
            <View className="add-btn-box">
              <View className={`at-icon at-icon-add and-btn ${addBtnType}`} onClick={this.onAdd}></View>
            </View>
            <View className={`zx-rz ${addBtnType}`}>
              <View className="zx-btn" onClick={this.goPage.bind(this, '/pages/brandJoining/franchiseInformation')} style={{'backgroundImage': `url('${_IMG_URL}/jiamengziyun-icon@2x.png')` }}>加盟咨询</View>
              <View className="rz-btn" onClick={this.goPage.bind(this, '/pages/brandJoining/brandEntry')} style={{'backgroundImage': `url('${_IMG_URL}/pinpairuzhu-icon@2x.png')` }}>品牌入驻</View>
            </View>
          </View>
        </View>
      </Block>
    );
  }
}

export default connect()(BrandJoining) as ComponentClass;
