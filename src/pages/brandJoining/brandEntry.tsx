import React, { Component, ComponentClass } from 'react';
import { Block, View, Text, Input, Button } from '@tarojs/components';
import { AtTextarea } from 'taro-ui';
import { Props, State } from '@/pages/brandJoining/type'
import Navigator from '@/utils/navigate/Navigator';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
  textarea: string
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
// 品牌入驻
class BrandEntry extends Component<IProps, OwnState> {
  state: OwnState = {
    textarea: '发打发士大夫发射点发射点风格的歌了亲爱的是啊手动阀手动阀'
  }
  UNSAFE_componentWillMount() {
  }
  componentDidMount() {
  }
  componentDidShow() {
  }
  textareaChange = (value) => {
    this.setState({
      textarea: value
    })
  }
  onSub = () => {
  }
  onConfirm = () => {
  }
  render() {
    return (
      <>
        <View className="global-input" >
          <Text className="label">*姓名</Text>
          <Input className="input" type='text' placeholder='请输入您的姓名' maxlength={20} />
        </View>
        <View className="global-input" >
          <Text className="label">*职位</Text>
          <Input className="input" type='text' placeholder='请输入您的职位' />
        </View>
        <View className="global-input">
          <Text className="label">*手机号</Text>
          <Input className="input" type='number' placeholder='请输入您的手机号' />
        </View>
        <View className="global-input" >
          <Text className="label">*品牌名称</Text>
          <Input className="input no-b" type='text' placeholder='请输入品牌名称' maxlength={30} />
        </View>
        <View className="global-textarea" >
          <View className="bg"></View>
          <Text className="label">备注信息</Text>
          <AtTextarea className="textarea" placeholderClass="textarea-placeholder" value={this.state.textarea} onChange={this.textareaChange.bind(this)} placeholder='' maxLength={1000} />
        </View>
        <View className="sub-btn-box">
          <Button className="sub-btn" onClick={this.onSub}>提交</Button>
        </View>
      </>
    );
  }
}

export default connect()(BrandEntry) as ComponentClass;
