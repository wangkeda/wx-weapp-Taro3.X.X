import React, { Component, ComponentClass } from 'react';
import { Block, View, Text, Input, Button } from '@tarojs/components';
import { AtTextarea } from 'taro-ui';
import { Props, State } from '@/pages/brandJoining/type'
import Navigator from '@/utils/navigate/Navigator';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
}
interface OwnState {
  // 自己要用的state放这
  textarea: string
}

type IProps =  OwnProps & Props;
// @connect(() => ({
// }))
// 加盟资讯
class FranchiseInformation extends Component<IProps, OwnState> {
  state: OwnState = {
    textarea: '1233312333312333333123333333123333331233312333312333333123331233331233333312333123333123333331233312333312333333'
  }
  UNSAFE_componentWillMount() {
  }
  componentDidMount() {
  }
  componentDidShow() {
  }
  textareaChange = (value) => {
    this.setState({
      textarea: value
    })
  }
  onSub = () => {
  }
  onConfirm = () => {
  }
  render() {
    return (
      <>
        <View className="global-input" >
          <Text className="label">*姓名</Text>
          <Input className="input" type='text' placeholder='请输入您的姓名' maxlength={20} />
        </View>
        <View className="global-input">
          <Text className="label">*手机号</Text>
          <Input className="input" type='number' placeholder='请输入您的手机号' />
        </View>
        <View className="global-input" >
          <Text className="label">*意向开店地区</Text>
          <Input className="input no-b" type='text' placeholder='请输入地区' maxlength={30} />
        </View>
        <View className="global-textarea" >
          <View className="bg"></View>
          <Text className="label">加盟需求</Text>
          <AtTextarea className="textarea" placeholderClass="textarea-placeholder" value={this.state.textarea} onChange={this.textareaChange.bind(this)} placeholder='请输入意向加盟品牌类型、偏好等信息' maxLength={1000} />
        </View>
        <View className="sub-btn-box">
          <Button className="sub-btn" onClick={this.onSub}>提交</Button>
        </View>
      </>
    );
  }
}

export default connect()(FranchiseInformation) as ComponentClass;
