export type StoreDispatch = {
  dataStr: ({
    payload: {
      star: number
    },
    callback: object,
  }) => void
}
export type StoreState = {
  homeData: {
    list: number[],
    star: number
  }
}
export type State = {
  filterData: any[]
  filterDropdownValue: any[],
  addBtnType: boolean,
}
export type Props = StoreDispatch & StoreState
