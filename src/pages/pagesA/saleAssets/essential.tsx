import React, { Component } from 'react';
import { View, Text, Image, Input } from '@tarojs/components';
import "@/styles/entrust.scss"

class Essential extends Component {
  componentDidMount() {
    console.log(`is componentDidMount`);
  }
  // 对应 onShow
  componentDidShow() {
    console.log(`is componentDidShow`);
  }
  // 对应 onHide
  componentDidHide() {
    console.log(`is componentDidHide`);
  }
  // 对应 onPullDownRefresh，除了 componentDidShow/componentDidHide 之外，
  // 所有页面生命周期函数名都与小程序相对应
  onPullDownRefresh() {}
  // 对应 onPullDownRefresh
  onReachBottom() {}
  componentWillUnmount() {
    console.log(`is componentWillUnmount`);
  }
  // 对应 onError
  componentDidCatchError() {
    console.log(`is componentDidCatchError`);
  }

  render() {
    return (
      <>
        <View className="tip-top">
          <Text className="ts">项目中的敏感信息不会对外公示，仅会对平台中审核通过的投资人单独展示。请放心填写。</Text>
          <View className="ps">ps:资料越完善，成功概率越高哦</View>
        </View>
        <View className="pic-item">
          <View className="at-icon at-icon-add" ></View>
          <View className="pic-text">添加照片</View>
          <View className="item-num">
            <Image mode="aspectFit" src={`${_IMG_URL}/id-num.png`} />
            <Text>编号：ZCJY20200965328</Text>
          </View>
        </View>
        <View className="global-input" >
          <Text className="label">*项目名称</Text>
          <Input className="input" type='text' placeholder='酒店名称 /公司名称 /物业名称' maxlength={20} />
        </View>
        <View className="time-picker" >
          <Text className="name ">日期</Text>
          <View>
             <Text className="picker-placeholder">请选择日期</Text>
            {/* //   : <Text className="picker-val">{dayjs(date).format('YYYY年MM月DD日')}</Text>
            // } */}
            <View className='at-icon at-icon-chevron-right' ></View>
          </View>
        </View>
        <View className="global-input" >
          <Text className="label">*项目名称</Text>
          <Input className="input" type='text' placeholder='酒店名称 /公司名称 /物业名称' maxlength={20} />
        </View>
        <View className="global-input" >
          <Text className="label">*详细地址</Text>
          <Input className="input" type='text' placeholder='请填写详细地址' maxlength={20} />
        </View>

      </>
    );
  }
}
export default Essential;
