import React, { Component } from 'react';
import { View, Text } from '@tarojs/components';

class PgaeDome extends Component {
  componentDidMount() {
    console.log(`is componentDidMount`);
  }
  // 对应 onShow
  componentDidShow() {
    console.log(`is componentDidShow`);
  }
  // 对应 onHide
  componentDidHide() {
    console.log(`is componentDidHide`);
  }
  // 对应 onPullDownRefresh，除了 componentDidShow/componentDidHide 之外，
  // 所有页面生命周期函数名都与小程序相对应
  onPullDownRefresh() {}
  // 对应 onPullDownRefresh
  onReachBottom() {}
  componentWillUnmount() {
    console.log(`is componentWillUnmount`);
  }
  // 对应 onError
  componentDidCatchError() {
    console.log(`is componentDidCatchError`);
  }

  render() {
    return <View className="index">Is Dome</View>;
  }
}
export default PgaeDome;
