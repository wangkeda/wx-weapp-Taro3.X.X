import React, { Component, ComponentClass } from 'react';
import { View } from '@tarojs/components';
import { Props, State } from '@/pages/details/type'
import Navigator from '@/utils/navigate/Navigator';
import Deal from '@/components/assetDetails/deal'
import InfoCard from '@/components/assetDetails/infoCard'
import FixedBox from '@/components/assetDetails/fixedBox'
import Liaison from '@/components/liaison'
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
import { getDocRect } from '@/utils/utils'

import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps =  OwnProps & Props;
// 资产出售
// @connect(() => ({
// }))
class DealIndex extends Component<IProps, OwnState & State> {
  readonly state: State = {
    btFixed: 0
  }
  async componentDidMount() {
    // Taro.setNavigationBarTitle({title: '1233'})
    const rect = await getDocRect("#bottom-fixed");
    this.setState({
      btFixed: rect.height
    })
  }
  componentDidShow() {
    
  }
  onShareAppMessage(res) {
    console.log(res);
    if (res.from === "button") {
      return {
        title:`自定义分享按钮`,
        path: `/pages/index/index`,
        imageUrl: 'https://img-md.veimg.cn/meadinindex/M00/00/DC/wKgyH1sLWBOAV35NAABZGipYALE942.jpg',
      };
    } else {
      return {
        title: "右上角分享事件",
        path: `/pages/index/index`,
        imageUrl: 'https://img-md.veimg.cn/meadinindex/M00/00/DC/wKgyH1sLWBOAV35NAABZGipYALE942.jpg'
      };
    }
  }
  render() {
    const list= [
      {
        key: 'name',
        name: '名称：',
        value: '杭州黄龙饭店',
      },
      {
        key: 'city',
        name: '所在城市：',
        value: '杭州黄龙饭店',
      },
      {
        key: 'address',
        name: '详细地址：',
        value: '浙江杭州浙江杭州浙江杭州浙江杭州浙江杭州浙江杭州',
      },
      {
        key: 'address',
        name: '经营类型：',
        value: '浙江杭州浙江杭州浙江杭州浙江杭州浙江杭州浙江杭州',
      },
    ]
    const { btFixed } = this.state
    const xmsm = '项目占地面积235平米，建筑面积1068平米，五层独栋，一楼餐厅、二至五楼客房，24间客房。酒店为毛坯交付，目前为土地，预计建成时间为2021-2022年，可根据投资人要求适当调整规划。酒店位于廊坊经济技术开发区核心位置，辐射廊坊国际会展中心，周边内500强企业众多，聚集众多高端商务人群，临近高速出口，紧邻北京大兴国际机场 ，距离北京首都国际机场和天津滨海国际机场车程都在1小时左右，可乘坐机场巴士沿京津塘高速往返。'
    return (
      <View className="have-fixed" style={{paddingBottom: btFixed}}>
        <Deal />
        <Liaison />
        <InfoCard title="物业信息" data={list} showType={1} showNum={8}  />
        <InfoCard title="交易信息" data={list} showType={1} showNum={8} />
        <InfoCard title="项目说明" data={[{explain: xmsm}]} showType={2} showNum={0} />
        <FixedBox showType="plain" />
      </View>
    );
  }
}
const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
})
export default connect(mapStateToProps, mapDispatchToProps)(DealIndex) as ComponentClass;
