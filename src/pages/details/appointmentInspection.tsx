import React, { Component, ComponentClass } from 'react';
import { View, Picker, Text, AtIcon, Image, Button  } from '@tarojs/components';
import dayjs from 'dayjs'
import { Props, State } from '@/pages/details/type'
import WPicker from '@/components/wPicker'
import Navigator from '@/utils/navigate/Navigator';
import Taro from '@tarojs/taro';
import { connect } from 'react-redux';
// import { getDocRect } from '@/utils/utils'

import './appointmentInspection.scss';

interface OwnProps {
  // 父组件要传的prop放这
}
interface OwnState {
  // 自己要用的state放这
  date: string,
  hour: string,
}

type IProps =  OwnProps & Props;
// 资产出售
// @connect(() => ({
// }))
class AppointmentInspection extends Component< OwnState> {
  readonly state: OwnState = {
    date: '',
    hour: '',
  }
  WPicker: any = {};
  componentDidMount() {
  }
  onDateChange(key, num, e) {
    console.log(e.value);
    this.setState({
      [key]: e.value
    })
  }
  getQuantum = () => {

  }
  onRef = (num, ref) => {
    this[`WPicker${num}`] = ref;
  }
  // onConfirm(type, e) {
  //         console.log(e)
  // }
  onCancel = () => {
  }
  showWP = (num) => {
    this[`WPicker${num}`].show()
  }
  goBack = () => {
    let pages = Taro.getCurrentPages()
    let prevPage = pages[pages.length-2];
    prevPage.setData({ //设置上一个页面的值
      tipsType: 'quantum'
    });
    Navigator.navigateBack();
  }
  render() {
    const {date, hour} = this.state
    return (
      <View>
        <View className="insp-tips" >Tips：为提高预约成功率，请尽量填写多个可安排的时间段</View>
        <View className="insp-title">请填写预约时间</View>
          <View className="insp-time-box">
            <View className="title ">可安排时间段1</View>
            <View className='page-body'>
              <View className='page-section'>
                  <WPicker
                      mode="date" 
                      startYear={dayjs().format('YYYY')} 
                      endYear="2029"
                      value="2020-03-07"
                      current={true}
                      fields="day"
                      confirm={this.onDateChange.bind(this, 'date', 1)}
                      cancel={this.onCancel}
                      disabledAfter={false}
                      onRef={this.onRef.bind(this, 1)}
                  >预约日期</WPicker>
                  <View className="time-picker" onClick={this.showWP.bind(this, 1)}>
                    <Text className="name ">日期</Text>
                    <View>
                      {
                        date === '' ? <Text className="picker-placeholder">请选择日期</Text>
                        : <Text className="picker-val">{dayjs(date).format('YYYY年MM月DD日')}</Text>
                      }
                      <View className='at-icon at-icon-chevron-right' ></View>
                    </View>
                  </View>
              </View>
              <View className='page-section'>
                <WPicker
                  mode="rangeHour" 
                  current={false}
                  confirm={this.onDateChange.bind(this, 'hour', 2)}
                  cancel={this.onCancel}
                  onRef={this.onRef.bind(this, 2)}
                >预约时间</WPicker>
                <View className="time-picker" onClick={this.showWP.bind(this, 2)} >
                  <Text className="name">时间</Text>
                  <View>
                    {
                      hour === '' ? <Text className="picker-placeholder">请选择具体时间段</Text>
                      : <Text className="picker-val">{hour}</Text>
                    }
                    <View className='at-icon at-icon-chevron-right' ></View>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View className="add-time">
            <Image className="add-icon" mode="aspectFit" src={`${_IMG_URL}/add.png`}> </Image>
            <View className="add-text">增加一个可安排时间</View>
          </View>
          <View className="sub-btn-box">
            <Button className="sub-btn" onClick={this.goBack}>提交预约申请</Button>
          </View>
      </View>
    );
  }
}
const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
})
// mapStateToProps, mapDispatchToProps
export default connect()(AppointmentInspection) as ComponentClass;
