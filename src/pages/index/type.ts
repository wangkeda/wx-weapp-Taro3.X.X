export type StoreDispatch = {
  fetchList: ({
    payload: {
      ishome: boolean
    },
    callback: object,
  }) => void
}
export type StoreState = {
  homeData: {
    list: [{
      type: number,
      name: string,
      num: string,
      keywordList: string[],
      price: string[]
    }],
    star: number
  }
}
export type Props = StoreState & StoreDispatch


export type State = {
  indexGrid: any[],
  downRefresh: boolean
}