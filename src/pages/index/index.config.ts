export default {
  navigationBarTitleText: '首页',
  navigationStyle: 'custom',
  enablePullDownRefresh: true,
  enableShareAppMessage: true,
};
