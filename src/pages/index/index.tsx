import React, { Component, ComponentClass } from 'react';
import { connect } from 'react-redux';
import Taro from '@tarojs/taro';
import Navigator from '@/utils/navigate/Navigator'
import { Props, State } from '@/pages/index/type'
import { View, Text, Image, Button, Block } from '@tarojs/components';
import MyTabBar from '@/components/myTabBar';
import DealCard from '@/components/assetCard/dealCard';
import FinancingCard from '@/components/assetCard/financingCard';
import HireCard from '@/components/assetCard/hireCard';
import { AtGrid, AtActivityIndicator } from "taro-ui"
import './index.scss';
class Index extends Component<Props, State> {
  readonly state: State = {
    downRefresh: false,
    indexGrid:[
      {value: '资产交易', image: `${_IMG_URL}/zichanjiaoyi.png`, url: '/pages/assetDeal/index'},
      {value: '资产租赁', image: `${_IMG_URL}/zichanzulin.png`, url: '/pages/capitalLease/index'},
      {value: '投融资', image: `${_IMG_URL}/yourongzi.png`, url: '/pages/investment/index'},
      {value: '品牌加盟', image: `${_IMG_URL}/pinpaijiameng.png`, url: '/pages/brandJoining/index'},
    ]
  }
  componentDidMount() {}

  componentWillUnmount() {}

  componentDidShow() {
    Taro.hideTabBar()
    this.props.fetchList({
      payload: {ishome: true},
      callback: (res) => {
        console.log(res);
        console.log(this.props);
        console.log('is fetchList callback');
      }
    })
  }
  componentDidHide() {}
  onShareAppMessage(res) {
    console.log(res);
    if (res.from === "button") {
      return {
        title:`自定义分享111按钮`,
        path: `/pages/index/index`,
        imageUrl: ''
      };
    } else {
      return {
        title: "右上角分222享事件",
        path: `/pages/index/index`,
        imageUrl: ''
      };
    }
  }
  onPullDownRefresh() {
    this.setState({
      downRefresh: true
    })
    setTimeout(() => {
      Taro.stopPullDownRefresh({
        success: () => {
          this.setState({
            downRefresh: false
          })
        }
      });
    }, 2000);
  }
  onReachBottom () {
    console.log('触底');
    this.props.fetchList({
      payload: {ishome: false},
      callback: (res) => {
        console.log(res);
        console.log(this.props);
        console.log('is fetchList callback');
      }
    })

  }
  onGrid = (item) => {
    Navigator.navigateTo({
      url: item.url
    })
  }

  render() {
    const { indexGrid, downRefresh } = this.state;
    const { list } = this.props.homeData
    return (
      <Block>
        <View className={`down-loading ${downRefresh}`}>
          <AtActivityIndicator color="#c9c9c9" mode="center" size={40}/>
        </View>
        <View className="banner-box">
          <Image className="index-banner-bg" mode="widthFix" src={`${_IMG_URL}/banner_bg.jpg`}></Image>
          <Image className="index-banner-logo" mode="widthFix" src={`${_IMG_URL}/banner_logo.png`} ></Image>
          <Text className="index-banner-text">商业空间资产交易与品牌加盟信息服务平台</Text>
        </View>
        <View className="index-menu-list">
          <AtGrid onClick={this.onGrid.bind(this)} mode='square' columnNum={4} hasBorder={false} data={indexGrid} />
        </View>
        <View className="index-recommendation">
          <Text className="recommendation-h1">项目推荐</Text>
        </View>
        <Button openType='share'>123123</Button>
        <View className="index-list">
          {
            list.map((item, index) => {
              return (
                <Block>
                  {item.type === 1 && <DealCard data={item} />}
                  {item.type === 2 && <FinancingCard data={item}  />}
                  {item.type === 3 && <HireCard data={item}  />}
                </Block>
              )
            })
          }
        </View>
        <MyTabBar />
      </Block>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    homeData: state.homeData
  }
}
const mapDispatchToProps = (dispatch) => ({
  fetchList: ({ payload, callback }) =>  dispatch({
    type: 'homeData/fetchList',
    payload: payload,
    callback: callback
  }),
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index) as ComponentClass
