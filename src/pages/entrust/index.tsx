import React, { Component, ComponentClass } from 'react';
import { Block, View, Text,  Image } from '@tarojs/components';
import Navigator from '@/utils/navigate/Navigator';
import Taro, { getCurrentInstance } from '@tarojs/taro';
import MyTabBar from '@/components/myTabBar';
import { connect } from 'react-redux';

import { StateType } from '@/models/accountModel';
import { ConnectProps, ConnectState } from '@/models/connect';
import RectWrap from '@/components/rectWrap';

import './index.scss';

interface OwnProps {
  // 父组件要传的prop放这
  value: number;
}
interface OwnState {
  // 自己要用的state放这
}

type IProps = StateType & ConnectProps & OwnProps;
@connect(({ account, loading }: ConnectState) => ({
  ...account,
  ...loading
}))
class Entrust extends Component<IProps, OwnState> {
  componentDidMount() {
  }
  componentDidShow() {
  }
  render() {
    // const { value } = this.props;
    return (
      <Block>
        <View className="label">请选择发布的委托类型：</View>
        <View className="list-option">
          <Image mode="aspectFill" src={`${_IMG_URL}/entrust1.jpg`}></Image>
          <Text className="title">资产出售</Text>
          <Text className="content">帮您快速寻找投资者，完成资产出售、项目转让</Text>
        </View>
        <View className="list-option">
          <Image mode="aspectFill" src={`${_IMG_URL}/entrust2.jpg`}></Image>
          <Text className="title">资产出租</Text>
          <Text className="content">帮您快速寻找意向承租者，完成您的空置物业出租</Text>
        </View>
        <View className="list-option">
          <Image mode="aspectFill" src={`${_IMG_URL}/entrust3.jpg`}></Image>
          <Text className="title">项目融资</Text>
          <Text className="content">帮您快速寻找意向投资者，满足您的公司融资需求</Text>
        </View>
      </Block>
      
    );
  }
}

export default Entrust as ComponentClass;
