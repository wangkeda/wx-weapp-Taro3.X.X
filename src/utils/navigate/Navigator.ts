import Taro from '@tarojs/taro';

const MAX_VALUE = 10 //实践发现，navigateTo成功回调之后页面也并未完全完成跳转，故将跳转状态短暂延长，单位：ms

/**
 * 导航器
 * 由于小程序只支持最多10级页面，但需求上希望维护更长的历史栈，故自行维护完整历史栈并改写默认导航操作
 */
const Navigator = {
  /*
   * desc: 跳转页面
   * param:
   *  obj: {
   *    url: ''  //页面在app.json中的路径，路径前不要加'/'斜杠（也可以加，做了兼容）
   *    data: {}  //需要携带的参数，统一通过缓存携带
   *  }
   */
  navigateTo: function(obj) {
    var pages = Taro.getCurrentPages(), //页面栈
      len = pages.length,
      dlt = 0,
      target = '/' + obj.url.replace(/^\//, ''), //如果有，将第一个‘/’去掉，然后再补上（开发者习惯不同，有些人会给url加/，有些则忘了，兼容处理
      navigation_key = target.replace(/\//g, '_') //存储数据的下标，每个页面由自己的存储key，保证了页面间数据不会相互污染
      console.log(pages)
    //查找目标页在页面栈的位置
    for (var i = 0; i < len; i++) {
      if (pages[i].route == target) {
        //
        dlt = i + 1 //目标页在栈中的位置
        break
      }
    }
    //保存数据
    //由于navigateBack()回到指定页面，不会重新执行onLoad事件，所以加个标兵。
    //只有在isLoad = true;时，才会接收参数并执行类onLoad事件
    var nData = Object.assign({ referer: pages[len - 1].route, _is_load: true }, obj.data || {})
    Taro.setStorageSync(navigation_key, JSON.stringify(nData))
    if (!dlt) {
      //页面不在栈中
      if (len < MAX_VALUE) {
        Taro.navigateTo({
          url: target,
        })
      } else {
        Taro.redirectTo({
          url: target,
        })
      }
    } else {
      Taro.navigateBack({
        delta: len - dlt,
      })
    }
  },
  navigateBack: function() {
    Taro.navigateBack({
      delta: 1,
    })
  },
  reLaunch: function(obj) {
    Taro.reLaunch(obj)
  },
  inPage: function(myOnLoad) {
    var pages = Taro.getCurrentPages()
    var navigation_key = pages[pages.length - 1].route.replace(/\//g, '_')
    //从其他页面跳转过来的，那么isLoad肯定为true，因为goPage中设置了。如果是用户点击左上角后退的，那么isLoad=false，因为下面设置了
    //获取数据
    try {
      var raw = Taro.getStorageSync(navigation_key)
      var options = JSON.parse(raw)
      if (options._is_load && myOnLoad) {
        //用户点击左上角后退时，不会执行myOnLoad，因为此时_is_load是undefined
        myOnLoad(options)
      }
      Taro.setStorage({
        //清除数据
        key: navigation_key,
        data: '', //这之后，_is_load是undefined了
      })
    } catch (e) {}
  },
}

export default Navigator
