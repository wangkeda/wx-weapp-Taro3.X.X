import Taro from '@tarojs/taro'
export const sysInfo: any  = Taro.getSystemInfoSync();
export const menuHight = (type: boolean, list: any[], height: number, navHight: number) => {
  const num = list.length;
  if (type) {
     return `${sysInfo.windowHeight - navHight}px`
  }
  if (num >= 5) {
    return `${5 * height}rpx`;
  }
  return `${num * height}rpx`;
}

export const getDocRect: any = async (node: string, time = 20) => {
  const rect = await new Promise((resolve) => {
    setTimeout(() => {
      const query = Taro.createSelectorQuery().select(node);
      query.boundingClientRect().exec((res) => {
        resolve(res[0]) 
      })
    }, time);
  })
  return rect
}