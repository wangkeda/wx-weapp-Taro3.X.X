import Taro from '@tarojs/taro'

// const NAVIGATOR_HEIGHT: number = 44
// const TAB_BAR_HEIGHT: number = 57

/**
 * 返回屏幕可用高度
 * // NOTE 各端返回的 windowHeight 不一定是最终可用高度（例如可能没减去 statusBar 的高度），需二次计算
 * @param {*} showTabBar
 */
export function getWindowHeight(showTabBar = true, unit = 'px') {
  const info = Taro.getSystemInfoSync()
  const { windowHeight, statusBarHeight, model } = info
  return windowHeight + unit
}

export function getPaddingTop(unit = 'px') {
  const info = Taro.getSystemInfoSync()
  const { model, statusBarHeight } = info
  let titleBarHeight = 48
  if (model.indexOf('iPhone X') !== -1 || model.indexOf('unknown') !== -1) {
    titleBarHeight = 44
  } else if (model.indexOf('iPhone') !== -1) {
    titleBarHeight = 44
  }
  return titleBarHeight + statusBarHeight + unit
}

/**
 * 返回屏幕可用高度
 * // NOTE 各端返回的 windowHeight 不一定是最终可用高度（例如可能没减去 statusBar 的高度），需二次计算
 * @param {*} showTabBar
 */
export function getWindowHeightNoTab(showTabBar = true, unit = 'px', showNavbar = true) {
  const info = Taro.getSystemInfoSync()
  const { windowHeight, statusBarHeight, model } = info
  return windowHeight + unit
}
