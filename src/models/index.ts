import accountModel from './accountModel';
import common from './common';
import home from './home';

export default [common, accountModel, home];
