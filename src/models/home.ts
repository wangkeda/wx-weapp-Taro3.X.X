import { homeReq } from "@/services/apiService"
const home = {
  namespace: 'homeData',
  state: {
    list: [],
    star: 0
  },
  effects: {
    *fetchList(action, { call, put } ) {
      console.log(action);
      const { payload, callback } = action
      const list = yield call(homeReq, payload)
      yield put({
        type: 'getList',
        payload: {list},
      })
      if (callback) {
        callback({isCallback: true})
      }
    }
  },
  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
    getList(state, { payload }) {
      return {
        ...state,
        list: state.list.concat(payload.list),
      }
    },
  }
}

export default home;