module.exports = {
  env: {
    NODE_ENV: '"development"',
  },
  defineConstants: {
    HOST: '""',
    _IMG_URL: '"https://f3-md.veimg.cn/meadinjuWX/img"'
  },
  mini: {},
  h5: {
    esnextModules: ['taro-ui'],
  },
};
